
Try it first!
-------------

If you own an android device (ARM), you may try some of the examples first,
which can be downloaded from here:

[Common Lisp and QML for Mobile](https://www.lights-of-holiness.eu/android/)



Acknowledgements
----------------

The build process defaults now (2020) to **64bit**.

This project requires *exactly* the following development environment:
(in order to match the pre-built EQL5 libraries for android)

* ECL 20.4.24 [official release](https://common-lisp.net/project/ecl/static/files/release/)
* Qt 5.13.2 [linux-x64-android](https://download.qt.io/archive/qt/5.13/5.13.2/)
* Android NDK version r19c

(The desktop EQL5, from which this port originates, is not strictly needed, at
least not to compile android apps; a **host ECL** will be sufficient for
cross-compiling, see below.)

N.B: You will *not* need to build the EQL5 shared libraries for android; the
cross-compiled versions are already included (see `lib/`).
But if you prefer to build them on your own, please see `build-eql5/`.

Since the included EQL5 android libraries have been built with the above
versions, you're required to have the exact same versions installed.

--

The minimum android **API** level (for Qt5.13) is **21**, which means that
the android version must be at least **5.0**.

If you absolutely need to support older android versions, please use an older
Qt version like Qt 5.12 (minimum API level 16), and see directory
`examples/my/android-4.2-fix` (since this hack for older android versions has
now been removed from all examples, because confusing if not needed).



Step 1: Install all Android development tools
---------------------------------------------

A good description of what you'll need can be found here:

[http://doc.qt.io/qt-5/androidgs.html](http://doc.qt.io/qt-5/androidgs.html)

You don't need Android Studio, so search for **command line tools** on the SDK
download page.

The SDK tools are expected to be found here: `/opt/android/sdk/`: you probably
want to install them in your home dir (for updates), and add a link from the
above location.



Step 2. Build cross-compiled ECL for Android
--------------------------------------------

To build the cross-compiled ECL **aarch64**, just use the 2 scripts included in
this project (see `scripts/`).

* extract ECL sources in e.g. `~/ecl`, renaming `ecl-20.4.24` to `android`
* copy the 2 scripts from `scripts/` to `~/ecl/android/`
* do (adapt the path to match your NDK directory):

```
  $ export ANDROID_NDK_TOOLCHAIN='/home/<username>/android/android-ndk-r19c/toolchains/llvm/prebuilt/linux-x86_64'
```

* run both scripts in order

```
   ./1-make-ecl-host.sh
   ./2-make-ecl-android.sh
```

Now you should have your cross-compiled ECL under `~/ecl/android/ecl-android/`,  
and your host ECL (for cross-compiling) under `~/ecl/android/ecl-android-host/`.

--

Optionally build cross compiled ECL for **armv7**, appending `-32` where
ambiguous (see below example paths).



Step 3. Settings in your `~/.bashrc`:
-------------------------------------

* environment variables

```
    JAVA_HOME             : path of your Java jdk
    ANDROID_NDK_TOOLCHAIN : path of NDK toolchain
    ANDROID_NDK_ROOT      : path of NDK root
    ECL_ANDROID           : path of your cross-compiled ECL
    QT_VERSION            : Qt5 version

    # for 32 bit (optional)
    ECL_ANDROID_32
```

* aliases

```
    ecl-android   : exe of host ECL (64 bit) for cross-compiling
    qmake-android : exe of qmake for android arm64 (included in Qt5)

    # for 32 bit (optional)
    ecl-android-32
    qmake-android-32
```

--

Example:

```
  # <snip ~/.bashrc>

  export JAVA_HOME='/home/<username>/android/jdk1.8.0_131'
  export ANDROID_NDK_TOOLCHAIN='/home/<username>/android/android-ndk-r19c/toolchains/llvm/prebuilt/linux-x86_64'
  export ANDROID_NDK_ROOT='/home/<username>/android/android-ndk-r19c'
  export ECL_ANDROID='/home/<username>/ecl/android/ecl-android'
  export QT_VERSION=5.13.2

  # optional
  export ECL_ANDROID_32='/home/<username>/ecl/android/ecl-android-32'

  alias ecl-android='/home/<username>/ecl/android/ecl-android-host/bin/ecl -norc'
  alias qmake-android='/home/<username>/Qt"$QT_VERSION"/Qt"$QT_VERSION"/android_arm64_v8a/bin/qmake'

  # optional
  alias ecl-android-32='/home/<username>/ecl/android/ecl-android-host/bin/ecl -norc'
  alias qmake-android-32='/home/<username>/Qt"$QT_VERSION"/Qt"$QT_VERSION"/android_armv7/bin/qmake'

  # </snip ~/.bashrc>
```

Please don't forget to pass `-norc` to your host ECL (otherwise it could
conflict with your standard ECL compiled files, e.g. ASDF).

(Use `source ~/.basrhc` to make new environment variables take effect
immediately in your current terminal session.)

--

Now you should be able to build the examples. Please note that only examples
**my** and **REPL** are prepared for both **32** and **64** bit builds, since
64 bit should be the default (as of 2020).

