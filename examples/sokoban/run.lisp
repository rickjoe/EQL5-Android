(load "lisp/3rd-party/sokoban")
(load "lisp/3rd-party/my-levels")
(load "lisp/qml-lisp")
(load "lisp/ui-vars")
(load "lisp/sokoban")

(progn
  (qsoko:run)
  (|showMaximized| qml:*quick-view*))
