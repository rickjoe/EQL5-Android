;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*accuracy*
   #:*animation*
   #:*heart-rate*
   #:*main*
   #:*remote-ip*
   #:*swank*
   #:*zoom-in*
   #:*zoom-out*))

(provide :ui-vars)

(in-package :ui)

(defparameter *main*       "main")       ; Rectangle           "qml/ext/HeartRate.qml"
(defparameter *swank*      "swank")      ; RoundButton         "qml/ext/Swank.qml"
(defparameter *zoom-in*    "zoom_in")    ; ScaleAnimator       "qml/ext/HeartRate.qml"
(defparameter *zoom-out*   "zoom_out")   ; ScaleAnimator       "qml/ext/HeartRate.qml"
(defparameter *animation*  "animation")  ; SequentialAnimation "qml/ext/HeartRate.qml"
(defparameter *accuracy*   "accuracy")   ; Text                "qml/ext/HeartRate.qml"
(defparameter *heart-rate* "heart_rate") ; Text                "qml/ext/HeartRate.qml"
(defparameter *remote-ip*  "remote_ip")  ; Tumbler             "qml/ext/Swank.qml"

