(load "dependencies")

(load "lisp/qml-lisp")
(load "lisp/ui-vars")
(load "lisp/thread-safe")
(load "package")
(load "lisp/ini")
(load "lisp/heart-rate-ini")
(load "lisp/heart-rate")

(progn
  (heart-rate:start)
  (qlater (lambda () (in-package :heart-rate))))
