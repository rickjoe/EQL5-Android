;;; see also 'dependencies.lisp'

(defsystem :heart-rate
  :serial t
  :components ((:file "lisp/qml-lisp")
               (:file "lisp/ui-vars")
               (:file "lisp/thread-safe")
               (:file "package")
               (:file "lisp/ini")
               (:file "lisp/heart-rate-ini")
               (:file "lisp/heart-rate")))
