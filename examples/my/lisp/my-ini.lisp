(in-package :my)

(qrequire :quick)

(defun start ()
  (eval:ini)
  (qml:ini-quick-view "qml/my.qml")
  (qconnect qml:*quick-view* "statusChanged(QQuickView::Status)" ; for reloading
            (lambda (status)
              (case status
                (#.|QQuickView.Ready|
                 (qml-reloaded))
                (#.|QQuickView.Error|
                 (qmsg (x:join (mapcar '|toString| (|errors| *quick-view*))
                               #.(make-string 2 :initial-element #\Newline)))))))
  (qlater (lambda () (eval:eval-in-thread "(help)" nil)))) ; show help

;;; REPL

(defun show-repl (show) ; called from QML
  (when show
    (q> |opacity| ui:*repl-container* 0)
    (q> |visible| ui:*repl-container* t))
  (dotimes (n 10)
    (q> |opacity| ui:*repl-container* (/ (if show (1+ n) (- 9 n)) 10))
    (qsleep 0.015))
  (unless show
    (q> |visible| ui:*repl-container* nil)))

(defvar *reload-url* "http://localhost:8080/")

(defun set-url (url)
  (when url
    (setf *reload-url* url)))

;;; load remote Lisp file

(let ((ini t))
  (defun eql::load* (file &optional url)
    "Load Lisp file from an url."
    (set-url url)
    (when ini
      (setf ini nil)
      (load "load-from-url"))
    (let ((remote-file (x:cc *reload-url* file (if (pathname-type file) "" ".lisp"))))
      (load-from-url remote-file)
      remote-file)))

(export 'eql::load* :eql)

;;; reload QML from remote

(defun reload-qml (&optional url)
  "Reload QML file(s) from an url."
  (set-url url)
  (let ((src (|toString| (|source| qml:*quick-view*))))
    (if (x:starts-with "qrc:/" src)
        (|setSource| qml:*quick-view* (qnew "QUrl(QString)"
                                            (x:string-substitute *reload-url* "qrc:/" src)))
        (qml:reload))
    (|toString| (|source| qml:*quick-view*))))

(defun qml-reloaded ()
  ;; re-ini
  )
