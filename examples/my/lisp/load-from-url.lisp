(in-package :my)

(require :ecl-curl)

(defun load-from-url (url)
  (multiple-value-bind (response headers stream)
      (loop
         (multiple-value-bind (response headers stream)
             (ecl-curl::url-connection url)
           (unless (member response '(301 302))
             (return (values response headers stream)))
           (close stream)
           (setf url (header-value :location headers))))
    (when (>= response 400)
      (error 'ecl-curl:download-error :url url :response response))
    (load stream)
    (close stream)))
