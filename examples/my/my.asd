;;; see also 'dependencies.lisp'

(defsystem :my
  :serial t
  ;; just here as an example for integrating Quicklisp libraries
  :depends-on (:iterate)
  :components ((:file "lisp/qml-lisp")
               (:file "lisp/ui-vars")
               (:file "lisp/eval")
               (:file "lisp/thread-safe")
               (:file "package")
               (:file "lisp/ini")
               (:file "lisp/dialogs")
               (:file "lisp/my-ini")
               (:file "lisp/my")))
