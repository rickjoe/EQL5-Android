# ECL, EQL5 libs

rm -fr   android-sources/libs/arm64-v8a
mkdir -p android-sources/libs/armeabi-v7a

cp ../../lib32/libeql5.so            android-sources/libs/armeabi-v7a/
cp ../../lib32/libeql5_quick.so      android-sources/libs/armeabi-v7a/
cp ../../lib32/libeql5_multimedia.so android-sources/libs/armeabi-v7a/
cp ../../lib32/libeql5_network.so    android-sources/libs/armeabi-v7a/
cp ../../lib32/libeql5_sql.so        android-sources/libs/armeabi-v7a/
cp ../../lib32/libeql5_svg.so        android-sources/libs/armeabi-v7a/
cp $ECL_ANDROID_32/lib/libecl.so     android-sources/libs/armeabi-v7a/

# comment out if not needed
cp ../../lib32/libcrypto.so          android-sources/libs/armeabi-v7a/
cp ../../lib32/libssl.so             android-sources/libs/armeabi-v7a/

# all prebuilt ECL libs (ASDF etc.)

mkdir -p android-sources/assets/lib/encodings

rm android-sources/assets/lib/*.fas

cp $ECL_ANDROID_32/lib/ecl-20.4.24/*.asd       android-sources/assets/lib/
cp $ECL_ANDROID_32/lib/ecl-20.4.24/*.fas       android-sources/assets/lib/
cp $ECL_ANDROID_32/lib/ecl-20.4.24/*.doc       android-sources/assets/lib/
cp $ECL_ANDROID_32/lib/ecl-20.4.24/encodings/* android-sources/assets/lib/encodings/

# for developing
mkdir -p android-sources/assets/lib/lisp
cp lisp/run.lisp android-sources/assets/lib/lisp/

# strip

$ANDROID_NDK_TOOLCHAIN/bin/arm-linux-androideabi-strip android-sources/assets/lib/*.fas

# a Swank version (with a small patch) that is guaranteed to work on android

mkdir -p android-sources/assets/lib/quicklisp/local-projects

rm -fr            android-sources/assets/lib/quicklisp/local-projects/slime
cp -r ../../slime android-sources/assets/lib/quicklisp/local-projects/
