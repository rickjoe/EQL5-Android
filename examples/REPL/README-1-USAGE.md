### First launch / prebuilt ECL libs

After launching the app for the first time, all prebuilt ECL libs will be
copied from `assets/` to the home directory (to be easily accessible and
replaceable, just in case).

Now you can `require` them as usual, e.g:

```
  (require :asdf)
  (require :sockets)
```

To get **Quicklisp**, simply eval:

```
  (eql:quicklisp)
```
(or just `:q` on the command line)



### Included ASDF libraries

The following ASDF libraries are already included, ready to use:
`:cl-ppcre`, `:iterate`, `:split-sequence`.



### Entering parens

Of course we need an easy way to enter `(` and `)`, independent from the
keyboard. Above the keyboard you find 2 buttons for this purpose. Holding the
`)` button will close all currently open parens.



### Copy, Paste, Eval one expression

(works both in the editor and the command line)

Tap and hold on the beginning (or inside) of an expression (e.g. on `defun`) to
select the whole expression, and to show the popup menu for copy and paste. If
you paste the expression to a different indentation level, the indentation will
be adapted automatically.

To eval the selected expression only, just click on the lambda button of the
popup menu.



### Copy result to global Clipboard

In order to use the result in another app, just enter `*` followed by RET, as
you would do to show the latest eval value.

This will copy the result to the clipboard.



### Moving the cursor

With the arrow buttons on the right above the keyboard you can move the cursor
in both the editor and the command line (where up and down will move it to the
beginning and end, respectively).

Tap-and-hold for moving to beginning/end of line (left/right) or file (up/down).



### Auto completion

Entering 2 spaces within half a second will start auto completion. It will
complete only a part, if ambiguous.

On abbreviations, like `m-v-b` for `multiple-value-bind`, it will complete the
whole word, preferring the first alphabetical match if ambiguous.

Please note that auto completion only works for a fixed set of CL symbols (and
**not** for user defined functions or variables).



### Search

Use `:?` on the command line to find a **regular expression** (or plain text)
in the editor:

```
  :? prin[1c] ; finds both PRIN1 and PRINC

  :? "hello"
```
To find the next match, just hit the [Return] button. The search will restart on
top after the last match.

If you only need to search for **plain text** (so you don't need to escape
certain characters), you can put this in your `.eclrc`:

```
  (setf editor:*plain-text-search* t)
```



### External keyboard

* keys **Up** and **Down** move in command line history
* key **Tab** switches focus between the edtior and the command line
* keys **Ctrl+E** (E for **E**xpression) selects s-expression (like tap-and-hold)
* keys **Ctrl+L** (L for **L**ambda) evals selected (single) expression



### Loading remote files into the editor

After connecting via USB, run this from the desktop (in the directory where
your Lisp files are located):
```
  adb reverse tcp:8080 tcp:8080 # reverse port forwarding
  ./web-server.sh               # see sources
```
Now load the file like this:
```
  (load* "example" t) ; pass T to load it into the editor
```



### Tips

If you want to print some values while your code is running, just use function
```
  (ed:pr x) ; short for (editor:append-output x), see function definition
```
The argument x can be of any type and will be printed immediately (think of a
long running loop).
You may also define the text color etc. (see function definition).

You can see your command history by simply opening file
`.eql5-lisp-repl-history` in your home directory.

When you enter a new file name for saving a file, you may add a path with new
directories: they will be created before saving the file.

If the output window has too much contents (thousands of lines, may happen
accidentally), and is becoming slow, remember that you're using the same Lisp
image where your REPL lives in, so you can clear it directly like this:

```
  (in-package :editor)

  (q! |clear| *qml-output*)
```
(or just `:c` on the command line)

For trivial debugging (or as an output window) you can use a simple (blocking)
**message box** from both Lisp and QML (remember that `qmsg` accepts any Lisp
value, converting it to a string if necessary; here: an `int` and a JS `list`):

**Lisp**
```
  (eql:qmsg 42) ; returns its argument, just like PRINT
```

**QML**
```
  import EQL5 1.0

  Lisp.call("qmsg", ["var x", x, "var y", y])
```

The message box will add a vertical scrollbar if necessary. To control the Lisp
object output width, simply use `*print-right-margin*` (number of characters).
Note also that you may use simple `html` tags in the message box, like:

```
  (qmsg "<font color='red'><b>BINGO!</b></font>")
```

To evaluate JS code (available in QML), use function `qml:js`:

```
  (qml:js nil "keyboardHeight()")                 ; JS function defined in 'repl.qml'

  (qml:js nil "Qt.inputMethod.keyboardRectangle") ; global Qt property
```

The first argument is the `this` context of JS; `nil` means the root item.



### Logging

If you want to use logging on the device through `adb logcat`, you can use:

```
  ;; Lisp
  (eql:qlog "message")
  (eql:qlog 1 "plus" 2 "gives" 6/2)
  (eql:qlog "x: ~A y: ~A" x y)

  // QML
  console.log("message")
```

The logged lines will contain `[EQL5]`, so you can use:

```
  adb logcat -s "[EQL5]"
```



### OpenSSL support

Both `libcrypto.so` and `libssl.so` are included, but for some reason they
won't be loaded automatically when requested (on android), so you need to put
this in your `.eclrc` in order to load them automatically at startup (or you
make a clean install, then `.eclrc` will be copied automatically):

```
  (ffi:load-foreign-library "libcrypto.so")
  (ffi:load-foreign-library "libssl.so")
```
Now Quicklisp libraries like **drakma** (which depend on ssl) should work.

(If you get an error about not being able to load `libssl.so` after installing
`cl+ssl`, just close the app and restart it: this should solve the problem.)



### QWidget support

Since this is just a port of EQL5/Desktop, you can obviously use `QWidget` and
friends (although they are not really designed for mobile).

As a test, just copy `examples/2-clock.lisp` from EQL5 to your mobile device
and run it (it will look exaclty the same as on the desktop). To close a shown
widget, just use the android back button.

Please note that the above example is not made for reloading, for this you
would need to change `(defvar *clock*` in `(defparameter *clock*`.



### Slime

Please see [README-SLIME](README-3-SLIME.md)
