import QtQuick 2.10
import QtQuick.Controls 2.10
import "." as Ext
import EQL5 1.0

Rectangle {
    id: debugDialog
    objectName: "debug_dialog"
    color: "#f0f0f0"
    visible: false

    Column {
        anchors.fill: parent

        Ext.MenuBack {
            id: menuBack
            label: "Debug Dialog"

            onPressed: {
                debugInput.text = ":q"
                Lisp.call("dialogs:exited")
            }
        }

        TextField {
            id: debugInput
            objectName: "debug_input"
            width: parent.width
            font.family: "Hack"
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
            text: ":q"

            onAccepted: Lisp.call("dialogs:exited")
        }

        Text {
            id: label
            width: parent.width
            leftPadding: 8
            rightPadding: 8
            topPadding: 8
            bottomPadding: 8
            font.family: "Hack"
            font.pixelSize: debugInput.font.pixelSize - (main.isPhone ? 4 : 2)
            text: ":r1 etc. restart / :h help / :q quit"
        }

        Rectangle {
            id: line
            width: parent.width
            height: 1
            color: "#d0d0d0"
        }

        ListView {
            id: debugText
            objectName: "debug_text"
            width: parent.width
            height: main.availableHeight()
                    - menuBack.height
                    - debugInput.height
                    - label.height
                    - line.height
            contentWidth: parent.width * 5
            clip: true
            model: debugModel
            flickableDirection: Flickable.HorizontalAndVerticalFlick

            delegate: Text {
                padding: 8
                textFormat: Text.PlainText
                font.pixelSize: debugInput.font.pixelSize - (main.isPhone ? 2 : 0)
                font.family: "Hack"
                font.bold: blockBold
                text: blockText
                color: blockColor
            }
        }

        ListModel {
            id: debugModel
            objectName: "debug_model"

            function addBlock(text, color, bold) {
                append(dict(text, color, bold))
                debugText.positionViewAtEnd()
            }

            function dict(text, color, bold) {
                return {
                    blockText: text,
                    blockColor: color,
                    blockBold: bold
                }
            }
        }
    }
}
