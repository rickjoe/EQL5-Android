import QtQuick 2.10
import QtQuick.Controls 2.10
import Qt.labs.folderlistmodel 2.10
import "." as Ext
import EQL5 1.0

Rectangle {
    id: fileBrowser
    objectName: "file_browser"
    visible: false

    // header, footer need this
    property Rectangle header
    property TextField path

    property bool editMode: false
    property string editFrom

    function urlToString(url) { return url.toString().substring("file://".length) }

    ListView {
        id: folderView
        objectName: "folder_view"
        anchors.fill: parent
        delegate: Ext.FileDelegate {}
        currentIndex: -1 // no initial highlight
        headerPositioning: ListView.OverlayHeader
        footerPositioning: ListView.OverlayHeader

        property var colors: ["white", "#f0f0f0"]

        model: FolderListModel {
            id: folderModel
            objectName: "folder_model"
            showDirsFirst: true
            showHidden: true
            nameFilters: ["*.lisp", "*.lsp", "*.asd", "*.exp", "*.sexp", "*.fas", "*.fasb", "*.fasc", ".eclrc", ".eql5-lisp-repl-history"]
        }

        header: Rectangle {
            id: header
            width: fileBrowser.width
            height: headerColumn.height
            z: 2
            color: "#f0f0f0"

            Component.onCompleted: fileBrowser.header = header // header, footer need this

            Column {
                id: headerColumn

                Ext.MenuBack {
                    id: menuBack

                    onPressed: {
                        Lisp.call("dialogs:set-file-name", "")
                        Lisp.call("dialogs:pop-dialog")
                    }

                    Row {
                        id: buttonRow
                        spacing: 4
                        anchors.horizontalCenter: parent.horizontalCenter

                        // one directory up
                        Ext.DialogButton {
                            text: "\uf062"
                            onClicked: Lisp.call("dialogs:set-file-browser-path", urlToString(folderModel.parentFolder))
                        }

                        // storage
                        Ext.DialogButton {
                            text: "\uf0c7"
                            onClicked: Lisp.call("dialogs:set-file-browser-path", ":storage")
                        }

                        // documents
                        Ext.DialogButton {
                            text: "\uf0f6"
                            onClicked: Lisp.call("dialogs:set-file-browser-path", ":data")
                        }

                        // home
                        Ext.DialogButton {
                            text: "\uf015"
                            onClicked: Lisp.call("dialogs:set-file-browser-path", ":home")
                        }
                    }
                }

                Ext.TextField {
                    id: path
                    objectName: "path"
                    width: fileBrowser.width
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
                    text: urlToString(folderModel.folder)

                    Component.onCompleted: fileBrowser.path = path // header, footer need this

                    onFocusChanged: if(focus) { cursorPosition = length }

                    onAccepted: {
                        if(fileBrowser.editMode) {
                            Lisp.call("dialogs:rename-file*", fileBrowser.editFrom, path.text)
                            fileBrowser.editMode = false
                        } else {
                            Lisp.call("dialogs:set-file-name", text)
                        }
                    }
                }
            }

            // edit mode
            Ext.DialogButton {
                id: fileEdit
                objectName: "file_edit"
                anchors.right: parent.right
                contentItem: Text {
                    id: editButton
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "\uf044"
                    color: fileBrowser.editMode ? "red" : "#007aff"
                    font.pixelSize: 24
                }

                onClicked: fileBrowser.editMode = !fileBrowser.editMode
            }
        }

        Row {
            y: header.height + (main.isPhone ? 7 : 10)
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 20
            visible: path.focus

            // cursor back
            Ext.ArrowButton {
                opacity: 0.15
                text: "\uf137"

                onPressed:      path.cursorPosition--
                onPressAndHold: path.cursorPosition = 0
            }

            // cursor forward
            Ext.ArrowButton {
                opacity: 0.15
                text: "\uf138"

                onPressed:      path.cursorPosition++
                onPressAndHold: path.cursorPosition = path.length
            }
        }

        footer: Rectangle {
            width: fileBrowser.width
            height: itemCount.height + 4
            z: 2
            color: "lightgray"
            border.width: 1
            border.color: "gray"

            Row {
                anchors.fill: parent

                Text {
                    id: itemCount
                    text: Lisp.call("cl:format", null, " ~D item~P", folderModel.count, folderModel.count)
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }
    }
}
