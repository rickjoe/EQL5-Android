import QtQuick 2.10
import QtQuick.Controls 2.10

Button {
    width: main.isPhone ? 40 : 60
    height: main.isPhone ? 37 : 55
    font.family: fontAwesome.name
    font.pixelSize: main.isPhone ? 25 : 36
    focusPolicy: Qt.NoFocus
}
