import QtQuick 2.10
import QtQuick.Controls 2.10

Button {
    width: main.isPhone ? 30 : 42
    height: width
    font.family: fontAwesome.name
    font.pixelSize: main.isPhone ? 20 : 28
    focusPolicy: Qt.NoFocus
}
