(require :ecl-curl)

(defun eql::curl (url)
  (multiple-value-bind (response headers stream)
      (loop
         (multiple-value-bind (response headers stream)
             (ecl-curl::url-connection url)
           (unless (member response '(301 302))
             (return (values response headers stream)))
           (close stream)
           (setf url (header-value :location headers))))
    (when (>= response 400)
      (error 'ecl-curl:download-error :url url :response response))
    (let ((byte-array (make-array 0 :adjustable t :fill-pointer t
                                  :element-type '(unsigned-byte 8))))
      (x:while-it (read-byte stream nil nil)
        (vector-push-extend x:it byte-array))
      (close stream)
      byte-array)))
