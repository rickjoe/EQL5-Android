
### Prepare

You need to prepare your android development environment first, as described
in [README-PREPARE](../../README-PREPARE.md).

(see also [known issues](http://wiki.qt.io/Qt_for_Android_known_issues))



### Notes

This example assumes you already tried `../tic-tac-toe` (more detailed
description).

This app has a variable screen orientation. To make it fixed, you can change
this property in
[android-sources/AndroidManifest.xml](android-sources/AndroidManifest.xml):

```
  android:screenOrientation="portrait"
```



### Make (cross-compile)

N.B: Always run script `clean-lisp-files.sh` after changing your ECL version!

**64 bit**

```
  ./1-copy-libs.sh              # copy EQL5, ECL, prebuilt ECL libs (ASDF...)

  ecl-android --shell make.lisp # note 'ecl-android'
  qmake-android repl.pro        # note 'qmake-android'
  make
```

**32 bit**

```
  ./1-copy-libs-32.sh              # copy EQL5, ECL, prebuilt ECL libs (ASDF...)

  ecl-android-32 --shell make.lisp
  qmake-android-32 repl32.pro      # note 'repl32.pro'
  make
```

To force recompilation of all files (e.g. after upgrading ECL), pass `-f`
as last argument to the `ecl-android` command.

If you get strange compile errors from Qt5, just run this before `qmake...`:

```
  rm -fr tmp
  rm Makefile*.*
```



### Build APK file

The following script will build a full version, including all of Qt5 (both
widgets and QML), because the **ministro** service for the Qt libs seems not
to work on some devices:

(Please note that copying the 32/64 Manifest file is only necessary if you
switch from the 32bit build to the 64bit one, or vice versa.)

**64 bit**
```
  ./2-build-apk.sh
```

**32 bit**
```
  ./2-build-apk-32.sh
```

This will build the APK, which needs to be copied and installed manually
(the reason is that the `--install` option would uninstall all app files,
including eventual previously installed Quicklisp libs etc.).

In order to set the app icons (3 different resolutions) and the app name
you need to run:

```
  $ qtcreator android-sources/AndroidManifest.xml
```

(The above manifest file is a copy of `android-build/AndroidManifest.xml`,
created during the first build of the apk.)

Please see also note in example `../my/README.md` about the Play Store.



### Desktop notes

Please see note in `qml/ext/Flickable.qml` about a **possible Qt bug**
(desktop only).

To run this example on the desktop, do:

```
  $ eql5 run.lisp
```

Or in Slime run this, then connect from Emacs:

```
  $ eql5 ~/slime/eql-start-swank.lisp run.lisp
```

N.B: Don't use command line option `-qtpl`, as this would interfere with the
debug dialog.

Please note also that any runtime bug in the Lisp code of this example itself,
when running from a console on the desktop, will cause an **infinite recursion
error loop**; this seems to happen because of the combination of the following:

 * re-binding of all streams
 * debug dialog (circumventing the console debugger)
 * eval running in its own thread

The above means that debugging this example running it directly from a console
on the desktop is not possible: you need to run it from Slime instead (please
see EQL5 Slime docu).



### Reload QML files from mobile device

You can reload the QML files directly from the REPL, editing them on your PC.

Switch to the REPL example directory, and run (after connecting the device
through USB):

```
  $ adb reverse tcp:8080 tcp:8080 # for below; requires android >= 5
  $ ./web-server.sh               # needs python3
```

Edit e.g. `qml/repl.qml` on the PC and save it; on the android REPL, eval:

```
  (editor:reload-qml)
```

(Reloading is possible because `QQuickView` accepts any URL as source file.)

**Hint**: search the web for `qml-mode` for Emacs, or just use QtCreator,
which is perfect for editing QML files.



### ASDF

The new integration (added 2020) should work for all ASDF systems supported
on mobile. (I tested successfully with e.g. `:ironclad`.)

For every dependency of your ASDF system (see `repl.asd`), you should add a
line in `dependencies.lisp` how to load it (usually via Quicklisp).

If some parts of your app depend on ECL contribs, like `:sockets`, you need
to build a `*.fas` file for them (see below), and load it only after program
startup (that is, after function `copy-asset-files` from `lisp/ini.lisp` has
been called). Otherwise the parts depending on ECL contribs will fail to load.

So, if you want to build an additional `*.fas` file, see `libs.asd` and
`dependencies-libs.lisp` (e.g. for building a library to be loaded after
program startup, that is, when all ECL assets have already been copied to the
app dir, so you can require e.g. `:sockets` etc),

For building the `*.fas` file, run:
```
  ecl-android --shell make-libs.lisp
```
Now you should find the resulting file `android-sources/assets/lib/libs.fas`.
It can then be loaded (*after* program startup, in order to have the ECL
contribs available) with a trivial:
```
  (load "libs")
```
Please note that you need to `require` the used ECL contribs before loading.
In the case of `:drakma`, this would be (the OpenSSL libs need to be provided
by the app and loaded manually):
```
  (require :sockets)
  (require :asdf)

  (ffi:load-foreign-library "libcrypto.so")
  (ffi:load-foreign-library "libssl.so")

  (load "libs")
```



### Slime

Please see [README-SLIME](README-3-SLIME.md)
