;;; A simple calculator
;;;
;;; (clc:run)

(defpackage :calculator
  (:nicknames :clc)
  (:use :common-lisp :eql)
  (:export
   #:*main*
   #:run))

(in-package :calculator)

(defvar *font*      (x:let-it (|font.QApplication|)
                      (|setPointSize| x:it 24)))

(defvar *font-mono* (qnew "QFont(QString,int)" "Hack" 24))

(defvar *main*      (qnew "QWidget"))

(defvar *real*      (qnew "QLabel"
                          "font" *font-mono*
                          "frameShape" |QFrame.Box|))

(defvar *float*     (qnew "QLineEdit"
                          "font" *font-mono*
                          "frame" nil
                          "readOnly" t))

(defvar *style-sheet*
  (list "QWidget     { background: white }"
        "QToolButton { background: lightsteelblue }"
        "QLabel      { background: '#e5e5e5' }"
        "QLineEdit   { background: lightyellow; border-width: 1px; border-style: solid; border-color: gray }"))


(defvar *precision*  0d0) ; f = float, d = double, l = long
(defvar *value1*     nil)
(defvar *value2*     nil)
(defvar *reset*      nil)
(defvar *operation*)
(defvar *decimals*)

(defun error-to-string (err)
  (let ((err (string-trim "#<a >" (write-to-string err :case :downcase))))
    (subseq err 0 (position #\# err))))

(defun funcall-protect (fun &rest args)
  (multiple-value-bind (val err)
      (ignore-errors (apply fun args))
    (or val
        (progn
          (|critical.QMessageBox| nil "Error" (error-to-string err))
          0))))

(defun display-number (n)
  (flet ((str (x)
           (format nil "~:D" x)))
    (x:when-it (funcall-protect (lambda (x) (float x *precision*)) n)
      (|setText| *float* (princ-to-string x:it)))
    (let* ((num (str (numerator n)))
           (den (str (denominator n)))
           (dif (- (length den) (length num))))
      (|setText| *real* (format nil "<u>~A~A</u><br>~A"
                                (if (plusp dif) (make-string dif) "")
                                num
                                den))
      (|setEnabled| (qfind-child *main* "blah") (= 1 (denominator n))))))

(defun clear-display ()
  (setf *value1* 0
        *decimals* nil)
  (display-number 0))

(defun words-clicked ()
  (qmsg (format nil "~R" *value1*)))

(defun digit-clicked ()
  (when *reset*
    (clear-display)
    (setf *reset* nil))
  (let ((clicked (parse-integer (|text| (qsender)))))
    (setf *value1* (if *decimals*
                       (+ (* clicked (expt 10 (- (incf *decimals*))))
                          *value1*)
                       (+ clicked
                          (* 10 *value1*)))))
  (display-number *value1*))

(defun back-clicked ()
  (when (and *decimals* (zerop *decimals*))
    (setf *decimals* nil))
  (setf *value1* (if *decimals*
                     (let ((n (expt 10 (decf *decimals*))))
                       (/ (truncate (* n *value1*)) n))
                     (truncate (/ *value1* 10))))
  (display-number *value1*))

(defun invert (operation)
  (setf *value1* (funcall-protect operation *value1*))
  (display-number *value1*))

(defun sign-clicked ()
  (invert '-))

(defun reci-clicked ()
  (invert '/))

(defun point-clicked ()
  (setf *decimals* 0))

(defun clear-clicked ()
  (setf *value2* nil)
  (clear-display))

(defun operate ()
  (x:when-it (funcall-protect *operation* *value2* *value1*)
    (setf *value2* x:it)
    (display-number *value2*)))

(defun operation-clicked ()
  (if *value2*
      (operate)
      (setf *value2* *value1*))
  (setf *operation* (intern (|text| (qsender)))
        *reset* t))

(defun equal-clicked ()
  (when *value2*
    (operate)
    (shiftf *value1* *value2* nil)
    (setf *reset* t)))

(defun close-clicked ()
  (|hide| *main*))

;;; UI

(defmacro connect-clicked (&rest args)
  `(progn
     ,@(loop :for arg :in args :collect
          `(qconnect ,arg "clicked()" ',(intern (string-upcase (format nil "~A-clicked" arg)))))))

(let (policy)
  (defun size-policy-expanding ()
    (or policy (setf policy (qnew "QSizePolicy(...)" |QSizePolicy.Expanding| |QSizePolicy.Expanding|)))))

(defun ini ()
  (flet ((btn ()
           (qnew "QToolButton"
                 "font" *font*
                 "sizePolicy" (size-policy-expanding))))
    (|setStyleSheet| (qapp) (x:join *style-sheet*))
    (let* ((layout* (|layout| *main*))
           (layout (if (qnull layout*) ; for multiple ini
                       (qnew "QGridLayout(QWidget*)" *main*)
                       (qt-object-? layout*)))
           (digits (make-array 10))
           (plus (btn)) (minus (btn)) (multiply (btn)) (divide (btn)) (reci (btn)) (sign (btn))
           (point (btn)) (clear (btn)) (back (btn)) (words (btn)) (equal (btn)) (close (btn)))
      (|setFixedHeight| close (second (|sizeHint| *float*)))
      (dotimes (n 10)
        (setf (svref digits n) (btn)))
      (x:do-with (|addWidget| layout)
        (reci     3 0)
        (divide   3 1)
        (multiply 3 2)
        (minus    3 3)
        (clear    3 4)
        (back     4 4)
        (words    5 4)
        (sign     6 3)
        (point    7 3)
        (close            0 0 1 5)
        (*real*           1 0 1 5)
        (*float*          2 0 1 5)
        (plus             4 3 2 1)
        (equal            6 4 2 1)
        ((svref digits 0) 7 0 1 3))
      (let ((n 0))
        (dotimes (r 3)
          (dotimes (c 3)
            (|addWidget| layout (svref digits (incf n)) (- 6 r) c))))
      (dolist (btn (list (list plus     "+")
                         (list minus    "-")
                         (list multiply "*")
                         (list divide   "/")
                         (list reci     "1/x"   "R")
                         (list sign     "+-"    "S")
                         (list point    ".")
                         (list clear    "AC"    "Delete")
                         (list back     "<<"    "Backspace")
                         (list words    "blah"  "B")
                         (list equal    "="     "Return")
                         (list close    "Close" "Escape")))
        (let ((w (first btn))
              (s (second btn)))
          (x:do-with (qset w)
            ("text" s)
            ("objectName" s)
            ("shortcut" (qnew "QKeySequence(QString)" (or (third btn) s))))))
      (dotimes (n 10)
        (let ((w (svref digits n))
              (s (princ-to-string n)))
          (x:do-with (qset w)
            ("text" s)
            ("objectName" s)
            ("shortcut" (qnew "QKeySequence(QString)" s)))))
      (dolist (w (list *float* *real*))
        (|setAlignment| w |Qt.AlignRight|))
      (dotimes (n 10)
        (qconnect (svref digits n) "clicked()" 'digit-clicked))
      (dolist (w (list plus minus multiply divide))
        (qconnect w "clicked()" 'operation-clicked))
      (connect-clicked clear back sign point reci words equal close)))) ; see macro above

(defvar %eval-once% (ini))

(defun run ()
  (clear-display)
  (|setFocus| *real*)
  (|show| *main*))

;;; run

(clc:run)
