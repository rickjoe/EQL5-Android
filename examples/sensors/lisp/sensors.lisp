;;; This is an extended port of QML example "accelbubble"

(in-package :sensors)

(defvar *max-x* nil)
(defvar *max-y* nil)

(si::trap-fpe t nil) ; ignore floating point overflows

;; we use a single shot timer for the sensor read interval, so we are
;; independent of both the data rate of the device (which could be very high,
;; a few hundret Hertz) and the data processing speed (which could be slow in
;; some cases)

(defun ini ()
  (setf *max-x* (- (q< |width| ui:*main*)  (q< |width| ui:*bubble*))
        *max-y* (- (q< |height| ui:*main*) (q< |height| ui:*bubble*)))
  (when (q< |active| ui:*accel*)   ; don't start if sensor isn't present
    (move-bubble))
  (when (q< |active| ui:*compass*) ; don't start if sensor isn't present
    (display-azimuth)))

;;; accelerometer

(defvar *accel-timer*   nil)
(defvar *accel-reading* nil)

(defun set-accel-reading () ; called from QML
  (setf *accel-reading* qml:*caller*))

(defun move-bubble ()
  (unless *accel-timer*
    (setf *accel-timer* (qnew "QTimer"
                              "interval" 1     ; for max. speed
                              "singleShot" t))
    (qconnect *accel-timer* "timeout()" 'move-bubble))
  (do-move-bubble)
  (|start| *accel-timer*))

(defconstant +const+ (/ 180 pi))

(defun do-move-bubble ()
  ;; this function is speed optimized, using neither QML-GET (Q<) nor QML-SET
  ;; (Q>); for this reason you can't have e.g. animations attached to the
  ;; 'bubble' item; if you need animations etc. on the 'bubble' item, use the
  ;; (slower) QML-SET (Q>)
  (when (find-quick-item ui:*accel*)     ; needed for QML reloading
    (let ((x (qget *accel-reading* "x"))  ; fastest way to read sensor data
          (y (qget *accel-reading* "y"))
          (z (qget *accel-reading* "z")))
      (flet ((pitch ()
               (- (* +const+ (atan (/ y (sqrt (+ (* x x) (* z z))))))))
             (roll ()
               (- (* +const+ (atan (/ x (sqrt (+ (* y y) (* z z)))))))))
        (let* ((bubble (find-quick-item ui:*bubble*))
               (new-x (+ (|x| bubble) (/ (roll) 10)))
               (new-y (- (|y| bubble) (/ (pitch) 10))))
          ;; direct/fast function calls (will not trigger eventual animations)
          (|setX| bubble (max 0 (min new-x *max-x*)))
          (|setY| bubble (max 0 (min new-y *max-y*))))))))

;;; compass

(defvar *compass-timer* nil)

(defun round* (x)
  (if x (truncate (+ 0.5 x)) 0))

(defun display-azimuth ()
  (unless *compass-timer*
    (setf *compass-timer* (qnew "QTimer"
                                "interval" 500
                                "singleShot" t))
    (qconnect *compass-timer* "timeout()" 'display-azimuth))
  ;; not speed optimized (long interval)
  (q> |text| ui:*azimuth*
      (princ-to-string (round* (q< |reading.azimuth| ui:*compass*))))
  (|start| *compass-timer*))

;;; interrupt timers

(defun %timers (fun)
  (dolist (timer (list *accel-timer* *compass-timer*))
    (when timer
      (funcall fun timer))))

(defun start-sensor-timers ()
  (%timers '|start|))

(defun stop-sensor-timers ()
  (%timers '|stop|))
