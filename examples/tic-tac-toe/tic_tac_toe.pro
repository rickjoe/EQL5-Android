QT             = widgets printsupport uitools quick quickwidgets qml androidextras
TEMPLATE       = app
CONFIG         += plugin no_keywords release
ECL_ANDROID    = $$(ECL_ANDROID)
INCLUDEPATH    += $$ECL_ANDROID/include ../../include
LIBS           += -L$$ECL_ANDROID/lib -lecl -L./build -lapp -L../../lib -leql5
PRE_TARGETDEPS += build/libapp.a
TARGET         = tic_tac_toe
DESTDIR        = ./android-build/libs/arm64-v8a
OBJECTS_DIR    = ./tmp/
MOC_DIR        = ./tmp/

SOURCES        += build/main.cpp

RESOURCES      = tic_tac_toe.qrc

ANDROID_PACKAGE_SOURCE_DIR = android-sources
