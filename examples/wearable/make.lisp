;;; This is a simple way to cross-compile without worrying about loading the
;;; object files compiled for the wrong architecture (a well known problem).
;;;
;;; ECL offers us the unique possibility to compile with both the byte-codes
;;; compiler and the C compiler.
;;;
;;; What we do here is to load the entire byte-compiled ASDF system beforehand.
;;;
;;; Then we say "good bye" to ASDF (which already did the heavy lifting), and
;;; simply cross-compile the (meantime) collected files manually, and combine
;;; them to a static library.

(load "../../utils/EQL5-symbols")

;;; *** STEP (1) **************************************************************

(si:install-bytecodes-compiler)

;;; load Quicklisp manually (we didn't load '.eclrc' for good reasons, because
;;; we are not standard ECL, only the host one for cross-compiling)

(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp" (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

;;; load byte-compiled ASDF system

(defvar *source-files* nil)

(defmethod asdf:perform ((o asdf:load-op) (c asdf:cl-source-file))
  (let ((file (namestring (asdf:component-pathname c))))
    (push (subseq file 0 (position #\. file :from-end t))
          *source-files*))
  (asdf::perform-lisp-load-fasl o c))

(load "dependencies")

(progn
  (push "./" asdf:*central-registry*)
  (asdf:load-system :wearable))

;;; load and prepare cross-compiler

(load "../../utils/cross-compile-32")

(setf *load-verbose* nil)
(setf *compile-verbose* t)
(setf c::*suppress-compiler-warnings* nil)
(setf c::*suppress-compiler-notes* nil)

(setf c::*compile-in-constants* t)

(pushnew :release *features*)

;; i18n
(x:when-it (probe-file "tr.lisp")
  (load x:it))

;;; *** STEP (2) **************************************************************

(require :cmp)

(setf *break-on-signals* 'error)

;;; cross-compile/link manually (byte-compiled version is already loaded)

(setf *source-files* (nreverse *source-files*))

(defvar *force-compile* (find "-f" (ext:command-args) :test 'string=))

(defun compile-files ()
  (dolist (file *source-files*)
    (let ((src (x:cc file ".lisp"))
          (obj (x:cc file ".o")))
      (when (or *force-compile*
                (> (file-write-date src)
                   (if (probe-file obj)
                       (file-write-date obj)
                       0)))
        (compile-file src :system-p t)))))

(compile-files)

(c:build-static-library "build/app"
                        :lisp-files (mapcar (lambda (file) (x:cc file ".o"))
                                            *source-files*)
                        :init-name "ini_app"
                        :epilogue-code '(watch:start))

;;; build 'curl.fas'

(require :ecl-curl)

(defvar *destination* "android-sources/assets/lib/curl")

(let ((*source-files* (list "package" "lisp/curl")))
  (compile-files)
  (c:build-fasl *destination*
                :lisp-files (mapcar (lambda (file) (x:cc file ".o"))
                                    *source-files*)))

(ext:run-program (x:cc (ext:getenv "ANDROID_NDK_TOOLCHAIN") "/bin/arm-linux-androideabi-strip")
                 (list (namestring (probe-file (x:cc *destination* ".fas")))))
