(in-package :watch)

(qrequire :quick)

(defun to-rad (a)
  (/ (* pi a) 180))

(defun sin* (a)
  (sin (to-rad a)))

(defun cos* (a)
  (cos (to-rad a)))

(defun button-pos (xy index width &optional extended)
  (let ((x? (string= "x" xy))
        (radius (/ width 2)))
    (funcall (if x? '+ '-)
             radius
             (* (- radius (if extended 3 0))
                (funcall (if x? 'sin* 'cos*)
                         (+ (if extended 59 37) 180 (* index 22)))))))

(defun button-text (index &optional extended)
  (if extended
      (nth index '("1/x" "sqrt" "isqrt" "exp" "log" "pi"
                   "sin" "sinh" "cos" "cosh" "tan" "tanh"))
      (case index
        (0  "C")
        (1  ".")
        (12 "±")
        (13 "=")
        (t  (princ-to-string (- index 2))))))

(defun button-color (index)
  (case index
    ((0 13) "lightpink")
    ((1 12) "lightgray")
    (t      "white")))

(defun button-clicked (text &optional color)
  (let ((ch (char text 0)))
    ;; animation
    (unless (string= "blah" text)
      (q! |stop| ui:*echo-anim*)
      (q> |text| ui:*echo* text)
      (q> |color| ui:*echo* (or color
                                 (if (find ch "C=") "lightpink")
                                 "white"))
      (q! |start| ui:*echo-anim*))
    ;; action
    (if (= 1 (length text))
        (x:if-it (position ch "+-×÷")
                 (clc:operation-clicked (intern (string (char "+-*/" x:it))))
                 (case ch
                   (#\C (clc:clear-clicked))
                   (#\= (clc:equal-clicked))
                   (#\. (clc:point-clicked))
                   (#\± (clc:sign-clicked))
                   (#\< (clc:back-clicked))
                   (t   (clc:digit-clicked (digit-char-p ch)))))
        (cond ((string= "1/x" text)
               (clc:reci-clicked))
              ((string= "blah" text)
               (clc:words-clicked))
              (t
               (clc:function-clicked (intern (string-upcase text))))))))
