;;;
;;; Includes everything for Quicklisp and Swank.
;;; Requires 'assets/lib/*' to contain all precompiled ECL contribs.
;;;

(in-package :eql)

(qrequire :network)

(defvar *assets-lib* "assets:/lib/")

(defun copy-asset-files (dir-name)
  "Copy all files from APK 'assets/lib/' to home path."
  (flet ((trim (name)
           (if (x:starts-with *assets-lib* name)
               (subseq name (length *assets-lib*))
               name)))
    (qlet ((dir "QDir(QString)" dir-name))
      (ensure-directories-exist (trim (x:cc dir-name "/")))
      (dolist (info (|entryInfoList| dir))
        (if (|isDir| info)
            (copy-asset-files (|filePath| info))
            (let* ((from (|filePath| info))
                   (to (trim from)))
              (unless (or (probe-file to)
                          (|copy.QFile| from to))
                (qmsg (format nil "Error copying asset file: ~S" from))
                (return-from copy-asset-files)))))))
  t)

(defun touch-file (name)
  (open name :direction :probe :if-does-not-exist :create))

(defun post-install ()
  (when (copy-asset-files *assets-lib*)
    (touch-file ".eql5-ini")
    :done))

(defun ini ()
  (ext:install-bytecodes-compiler)
  (let ((.eclrc ".eclrc"))
    (if (probe-file .eclrc)
        (load .eclrc)
        (touch-file .eclrc)))
  (unless (probe-file ".eql5-ini")
    (qlater 'post-install)))

;; Quicklisp setup (stolen from 'ecl-android')

(defun sym (symbol package)
  (intern (symbol-name symbol) package))

(defun quicklisp ()
  (unless (find-package :quicklisp)
    (require :ecl-quicklisp)
    (require :deflate)
    (require :ql-minitar)
    ;; replace interpreted function with precompiled one from DEFLATE
    (setf (symbol-function (sym 'gunzip :ql-gunzipper))
          (symbol-function (sym 'gunzip :deflate))))
  :quicklisp)

(export 'quicklisp)

;; Swank setup (stolen from 'ecl-android')

(defun swank/create-server (interface port dont-close style)
  (funcall (sym 'create-server :swank)
           :interface interface
           :port port
           :dont-close dont-close
           :style style))

(defun start-swank (&key (interface (ip-string)) log-events (load-contribs t) (setup t)
                         (delete t) (quiet t) (port 4005) (dont-close t) (style :spawn))
  "Pass :interface as an IP string, if you want to use a specific IP address."
  (unless (find-package :swank)
    (require :asdf)
    (funcall (sym 'load-system :asdf) :swank))
  (funcall (sym 'init :swank-loader)
           :load-contribs load-contribs
           :setup setup
           :delete delete
           :quiet quiet)
  (setf (symbol-value (sym '*log-events* :swank)) log-events)
  (eval (read-from-string "(swank/backend:defimplementation swank/backend:lisp-implementation-program () \"org.lisp.ecl\")"))
  (if (eql :spawn style)
      (swank/create-server interface port dont-close style)
      (mp:process-run-function
       "SLIME-listener"
       (lambda () (swank/create-server interface port dont-close style))))
  (qml:q> |text| ui:*swank* interface)
  (qlater (lambda () (in-package :watch))))

(defun ip-string ()
  "Tries to find the local WiFi address. If the result is not unique, an empty string is returned (no guesses)."
  (let (ip4)
    (dolist (ad (|allAddresses.QNetworkInterface|))
      (when (= (|protocol| ad) |QAbstractSocket.IPv4Protocol|)
        (let ((str (|toString| ad)))
          (when (x:starts-with "192.168.1." str)
            (return-from ip-string str))
          (unless (string= str "127.0.0.1")
            (push str ip4)))))
    (if (rest ip4)
        ""
        (first ip4))))

(defun stop-swank ()
  (when (find-package :swank)
    (funcall (sym 'stop-server :swank) 4005)
    :stopped))

(export 'start-swank)
(export 'stop-swank)

(defun ensure-android-permission (&optional (name "android.permission.WRITE_EXTERNAL_STORAGE"))
  "Check/request Android permission (for API level >= 23); the name defaults to \"android.permission.WRITE_EXTERNAL_STORAGE\". Returns T on granted permission."
  (! "checkPermission" (:qt *c-bridge*) name)) ; see ../build/load.h'

(export 'ensure-android-permission)

(defvar *c-bridge* (qfind-child (qapp) "c_bridge"))

;; permissions (android api >= 23)

(defun ensure-android-permission (&optional (name "android.permission.WRITE_EXTERNAL_STORAGE"))
  "Check/request Android permission (for API level >= 23); the name defaults to \"android.permission.WRITE_EXTERNAL_STORAGE\". Returns T on granted permission."
  (! "checkPermission" (:qt *c-bridge*) name)) ; see '../build/main.h'

(export 'ensure-android-permission)

(defun keep-screen-on ()
  (! "keepScreenOn" (:qt *c-bridge*))) ; see ../build/main.h'

(export 'keep-screen-on)

;; shell

(defvar *output* nil)

(export '*output*)

(defun shell (command)
  "Run shell commands; examples:
  (shell \"ls -la\")
  (shell \"ifconfig\")"
  (let ((tmp "tmp.txt"))
    (with-open-file (s tmp :direction :output :if-exists :supersede)
      (ext:run-program "sh" (list "-c" command)
                       :output s)) ; we need a file stream here
    (with-open-file (s tmp)
      (let ((str (make-string (file-length s))))
        (read-sequence str s)
        (fresh-line)
        (princ str)
        (setf *output* (x:split (string-trim '(#\Newline) str) #\Newline))))
    (delete-file tmp))
  (values))

(export 'shell)

;; convenience

(define-symbol-macro :q (quicklisp))
(define-symbol-macro :r (watch:reload-qml))

;; quit app

(defun back-pressed () ; called from QML
  (qquit))

(export 'back-pressed)

;; ini

(qlater 'ini)
