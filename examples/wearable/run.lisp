(load "dependencies")

(load "lisp/qml-lisp")
(load "lisp/ui-vars")
(load "lisp/thread-safe")
(load "package")
(load "lisp/ini")
(load "lisp/calc")
(load "lisp/wearable-ini")
(load "lisp/wearable")

(progn
  (watch:start)
  (qlater (lambda () (in-package :watch))))
