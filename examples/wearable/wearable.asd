;;; see also 'dependencies.lisp'

(defsystem :wearable
  :serial t
  :components ((:file "lisp/qml-lisp")
               (:file "lisp/ui-vars")
               (:file "lisp/thread-safe")
               (:file "package")
               (:file "lisp/ini")
               (:file "lisp/calc")
               (:file "lisp/wearable-ini")
               (:file "lisp/wearable")))
