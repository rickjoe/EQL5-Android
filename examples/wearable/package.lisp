(in-package :cl-user)

(defpackage :wearable
  (:nicknames :watch)
  (:use :cl :eql :qml)
  (:export
   #:reload-qml
   #:start))
