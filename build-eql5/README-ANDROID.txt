
INFO
----

If you don't want to use the included EQL5 libs from '../lib/' (or you want
to use a Qt version different from 5.13.2), just follow the below steps.



IMPORTANT NOTES
---------------

You will need EQL5 version >= 17.7.1 (check eql5 -v).

For developing android apps, simply stay with the exact same ECL/Qt5/EQL5
versions you used to build the cross-compiled EQL5 libs (to be binary
compatible).



CROSS-COMPILE EQL5
------------------

This will compile all supported eql5 libs (I'm assuming you already went
through '../../README-PREPARE.md').

- copy all files from this directory to your eql5 desktop installation, and
  switch to it:

  $ cp * ~/eql5/src/
  $ cd ~/eql5/src

Edit file 'android-make.lisp' and adapt the path to your 'EQL5-symbols.lisp'
file (first line); then do:

  $ ecl-android --shell android-make.lisp
  $ qmake-android android_eql5.pro
  $ make

The last step is stripping (to be ready for deployment):

  $ cd android-libs
  $ $ANDROID_NDK_TOOLCHAIN/bin/aarch64-linux-android-strip lib*

Finally copy the libs over to 'eql5-android':

  $ cp lib* ~/eql5-android/lib/

